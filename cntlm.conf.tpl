Username	[% user %]
Domain		[% domain %]
Password	[% password %]

Proxy		[% proxy %]

NoProxy		[% no_proxy %]

Listen		0.0.0.0:[% port %]
