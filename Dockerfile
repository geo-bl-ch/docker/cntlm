FROM alpine:3.18

RUN mkdir /data && \
    adduser -S -u 1001 -G root -h /data cntlm && \
    chown -R 1001:0 /data && \
    chmod -R g=u /data && \
    chgrp 0 /etc/passwd && \
    chmod g=u /etc/passwd && \
    apk update && \
    apk upgrade && \
    apk add cntlm bash perl-template-toolkit

COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/
COPY --chown=1001:0 cntlm.conf.tpl /etc/

ENV HOME=/data \
    CNTLM_PORT=8088 \
    NO_PROXY=localhost,127.0.0.*,10.*,192.168.*

EXPOSE 8088

WORKDIR /data

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD ["cntlm", "-f", "-c", "/data/cntlm.conf"]

USER 1001
